package com.example.demo.service;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {


  @Autowired
  private ProductRepository productRepository;


  @Override
  public Product createProduct(Product product) {
    return productRepository.save(product);
  }

  @Override
  public Product updateProduct(Product product) {
    Optional<Product> productDb = this.productRepository.findById(product.getProductId());

    if (productDb.isPresent()) {
      Product productUpdate = productDb.get();
      productUpdate.setProductId(product.getProductId());
      productUpdate.setProductDescription(product.getProductDescription());
      productUpdate.setPrice(product.getPrice());
      productRepository.save(productUpdate);
      return productUpdate;
    } else {
      throw new ResourceNotFoundException("Record not found with id : " + product.getProductId());
    }
  }

  @Override
  public List< Product > getAllProduct() {
    return this.productRepository.findAll();
  }


  @Override
  public Product getProductById(Integer productId) {

    Optional < Product > productDb = this.productRepository.findById(productId);

    if (productDb.isPresent()) {
      return productDb.get();
    } else {
      throw new ResourceNotFoundException("Record not found with id : " + productId);
    }
  }

  @Override
  public void deleteProduct(Integer productId) {
    Optional < Product > productDb = this.productRepository.findById(productId);

    if (productDb.isPresent()) {
      this.productRepository.delete(productDb.get());
    } else {
      throw new ResourceNotFoundException("Record not found with id : " + productId);
    }

  }

  @Override
  public List<Product> greaterThanGivenPrice(Long price) {
    return productRepository.findGreaterThanPrice(price);
  }
}