package com.example.demo.service;

import com.example.demo.model.Product;
import java.util.List;

public interface ProductService {
  Product createProduct(Product product);

  Product updateProduct(Product product);

  List< Product > getAllProduct();

  Product getProductById(Integer productId);

  void deleteProduct(Integer id);

  List<Product> greaterThanGivenPrice(Long price);
}