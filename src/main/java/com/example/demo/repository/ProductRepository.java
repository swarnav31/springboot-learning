package com.example.demo.repository;

import com.example.demo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
  @Query(value = "SELECT * FROM Products p WHERE p.price > :price", nativeQuery = true)
  public List<Product> findGreaterThanPrice(Long price);

  /**
   * https://stackoverflow.com/questions/10802798/spring-data-jpa-query-with-parameter-properties
   * Without nativeQuery, JPA-style query looks like:
      @Query(value = "SELECT p FROM Product p WHERE p.price > :price")
      public List<Product> findGreaterThanPrice(Long price);
   */
}
