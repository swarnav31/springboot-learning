package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "Products")
@AllArgsConstructor
@NoArgsConstructor
public class Product {
  @Id private Integer productId;
  private String productDescription;
  private Long price;
}
