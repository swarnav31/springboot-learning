package com.example.demo.controller;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Product;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity <List< Product >> getAllProduct() {
      return ResponseEntity.ok().body(this.productService.getAllProduct());
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity < Product > getProductById(@PathVariable Integer productId) {
      return ResponseEntity.ok().body(this.productService.getProductById(productId));
    }

    @PostMapping("/products")
    public ResponseEntity <Product> createProduct(@RequestBody Product product) {
      return ResponseEntity.status(HttpStatus.CREATED).body(this.productService.createProduct(product));
    }

    @GetMapping("/greaterThanPrice/{price}")
    public ResponseEntity <List<Product>> getProductsGreaterThanPrice(@PathVariable Long price) {
      return ResponseEntity.ok().body(this.productService.greaterThanGivenPrice(price));
    }

    @DeleteMapping("/products")
    public ResponseEntity<String> deleteProductById(@RequestParam Integer id) {
      this.productService.deleteProduct(id);
      return ResponseEntity.ok().body("Deleted");
    }

    @PutMapping("/products/{id}")
    public ResponseEntity < Product > updateProduct(@PathVariable Integer id, @RequestBody Product product) {
      product.setProductId(id);
      return ResponseEntity.ok().body(this.productService.updateProduct(product));
    }

    /**
     * We want to display suitable message when service layer throws ResourceNotFoundException
     * @return
     */
    @ExceptionHandler({ ResourceNotFoundException.class })
    public ResponseEntity<String> handleException() {
      return ResponseEntity.badRequest().body("No resource found");
    }
}
