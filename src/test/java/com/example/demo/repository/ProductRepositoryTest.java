package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;
import com.example.demo.model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import java.util.Optional;

@DataJpaTest
class ProductRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  public ProductRepository productRepository;

  @Test
  public void whenFindByID_thenReturnProduct() {

    // given
    Product tv = new Product(1, "TV", 3000L);
    entityManager.persist(tv);

    // when
    Optional<Product> found = productRepository.findById(tv.getProductId());

    // then
    assertThat(found.get().getProductDescription())
        .isEqualTo(tv.getProductDescription());
  }
}